1.0.0:
- Initial release

1.1.0:
- Added command `/votereset` to reset CMI votes in `/cmi votetop`

1.2.0:
- Added CItems `Firecracker` and `GoldenHead`
- Added crafting of GHeads

1.2.1:
- Fixed GHeads sometimes not giving absorption effect

1.2.2:
- Added `Speed I` for 7.5s on `GHead` consumption
- Removed hit cooldown bug when teleporting with enderpearl

1.2.3:
- Attempted to fix NPE on `InventoryClickEvent`

1.2.4:
- Rewrite to `GHead` recipe code

1.2.5:
- Added CItem `HomingBow`

1.2.6:
- Made `HomingBow` no longer target invisible players
- Made `HomingBow` only home after 2 ticks
- Made `HomingBow` only available to those with admin permissions

1.3.0:
- Added compass GUI menu
- Enforced instant compass on respawn
- Cancelled enderpearl damage in duels
- Added rightclicking of compass for instant duel request

1.3.1:
- Fixed casting error in `InventoryClickEvent`

1.3.2:
- Added new items `spleef`, `splegg` and `bowspleef` in `HelpGUI`
- Removed broken DuelsGUI references
- Changed `duels` in `HelpGUI` to execute a command instead

1.3.3:
- Compass now has to be rightclicked to open GUI
- Added custom damage delay

1.3.4:
- Changed damage delay to 1 tick (from 2)

1.3.5:
- Added new CItem `Egg`

1.3.6:
- Added new CItem `Rod Of Souls`
- Fixed bug where CItem `Egg` would duplicate forever and crash the server
- Fixed a bug where CItem `Egg` would get stuck in blocks

1.3.7:
- Removed 1-tick hit delay

1.4.0:
- Added NPC damage cooldown delay (10 ticks)

1.4.1:
- Made cooldown timer task for NPC damage async

1.4.2:
- Modified some `HelpGUI` items

2.0.0:
- Migrated to Maven
- Plugin rewrite for Eragon under Skaian <3

2.1.0:
- Added KitBattle compatibility
- Added Vampire ability (KitBattle)

2.2.0:
- Removed Speed from CItem `GHead`
- Disabled enderpearl damage
- Added Chemist ability (KitBattle)
- Added Thrower ability (KitBattle)

2.3.0:
- Removed KitBattle integration
- Added GHeads without lore
- Added CItem `HackerStick`

2.3.1:
- Fixed NPE with onGapple()
package com.zenya.eragonplus.citizens;

import org.bukkit.entity.*;

import java.util.HashMap;

public class NPCDamageCooldownTimer {
    private static NPCDamageCooldownTimer instance;
    private HashMap<Entity, Integer> cooldowns = new HashMap<>();

    public void setCooldown(Entity entity, Integer ticks) {
        if(ticks < 1) {
            cooldowns.remove(entity);
        } else {
            cooldowns.put(entity, ticks);
        }
    }

    public Integer getCooldown(Entity entity) {
        return cooldowns.getOrDefault(entity, 0);
    }

    public static NPCDamageCooldownTimer getInstance() {
        if(instance == null) {
            instance = new NPCDamageCooldownTimer();
        }
        return instance;
    }
}

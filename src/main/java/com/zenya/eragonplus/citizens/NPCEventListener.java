package com.zenya.eragonplus.citizens;

import com.zenya.eragonplus.EragonPlus;
import net.citizensnpcs.api.event.NPCDamageByEntityEvent;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

public class NPCEventListener implements Listener {
    private NPCDamageCooldownTimer timer = NPCDamageCooldownTimer.getInstance();

    @EventHandler
    public void onNPCDamageByEntityEvent(NPCDamageByEntityEvent event) {
        Entity npce = event.getNPC().getEntity();
        Integer ticksLeft = timer.getCooldown(npce);

        if(ticksLeft == 0) {
            timer.setCooldown(npce, 10);

            new BukkitRunnable() {
                @Override
                public void run() {
                    Integer ticksLeft = timer.getCooldown(npce);
                    timer.setCooldown(npce, --ticksLeft);

                    if(ticksLeft <= 0) this.cancel();
                }
            }.runTaskTimerAsynchronously(EragonPlus.getInstance(), 0, 1);
        } else {
            event.setCancelled(true);
        }
    }
}

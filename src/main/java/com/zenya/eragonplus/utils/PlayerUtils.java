package com.zenya.eragonplus.utils;

import com.zenya.eragonplus.EragonPlus;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

public class PlayerUtils {

    public static boolean hasMeta(Player player, String meta) {
        return player.hasMetadata(meta);
    }

    public static void setMeta(Player player, String meta, Object value) {
        player.setMetadata(meta, new FixedMetadataValue(EragonPlus.getInstance(), value));
    }

    public static void clearMeta(Player player, String meta) {
        if(hasMeta(player, meta)) {
            player.removeMetadata(meta, EragonPlus.getInstance());
        }
    }

    public static String getMetaValue(Player player, String meta) {
        if(!(hasMeta(player, meta)) || player.getMetadata(meta).size() == 0) return "";
        return player.getMetadata(meta).get(0).asString();
    }
}


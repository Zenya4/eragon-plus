package com.zenya.eragonplus.utils;

import com.zenya.eragonplus.EragonPlus;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.List;

public class ConfigManager {

    private static ConfigManager configManager;
    private Plugin plugin;
    private FileConfiguration config;

    public ConfigManager(Plugin plugin) {
        this.plugin = plugin;
        this.config = plugin.getConfig();

        if(!(this.getConfigExists())) {
            plugin.saveDefaultConfig();
        }
    }

    public boolean getConfigExists() {
        return  new File(plugin.getDataFolder(), "config.yml").exists();
    }

    public int getConfigVersion() {
        return config.getInt("config-version");
    }

    public String getMainWorld() {
        return config.getString("main-world");
    }

    public List<String> getNoDelayWorlds() {
        return config.getStringList("nodelay-worlds");
    }

    public List<String> getConsoleOnlyCommands() {
        return config.getStringList("console-only-commands");
    }

    public static ConfigManager getInstance() {
        if(configManager == null) {
            configManager = new ConfigManager(EragonPlus.getInstance());
        }
        return configManager;
    }
}

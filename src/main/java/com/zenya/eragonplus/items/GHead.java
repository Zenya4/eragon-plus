package com.zenya.eragonplus.items;

import com.zenya.eragonplus.storage.Trigger;
import com.zenya.eragonplus.utils.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class GHead implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case PLAYER_INTERACT: {
                PlayerInteractEvent e = (PlayerInteractEvent) event;
                Player player = e.getPlayer();

                if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !e.getAction().equals(Action.RIGHT_CLICK_AIR)) return;

                for(String permission : getPermissions()) {
                    if(player.hasPermission(permission)) {
                        player.removePotionEffect(PotionEffectType.ABSORPTION);
                        player.removePotionEffect(PotionEffectType.REGENERATION);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2400, 0));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 150, 1));
                        //player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 150, 0));

                        if(e.getItem().getAmount() > 1) {
                            e.getItem().setAmount(e.getItem().getAmount() - 1);
                        } else {
                            e.getPlayer().getInventory().removeItem(e.getItem());
                        }
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }
    }

    @Override
    public String getKey() {
        return "GHead";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.PLAYER_INTERACT);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("erangonplus.player");
        permissions.add("eragonplus.citem.ghead");
        return permissions;
    }

    @Override
    public String getName()  {
        return ChatColor.translateAlternateColorCodes('&', "&6Golden Head");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7An upgraded version of golden apples"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Right click to use"));
        desc.add("");
        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.GOLDEN_APPLE;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}

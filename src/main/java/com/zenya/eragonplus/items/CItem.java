package com.zenya.eragonplus.items;

import com.zenya.eragonplus.storage.Trigger;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface CItem {

    public void onTrigger(Event event, Trigger trigger);

    public String getKey();
    public List<Trigger> getTriggers();
    public List<String> getPermissions();
    public String getName();
    public List<String> getDescription();
    public Material getMaterial();
    public ItemStack getItem();
}


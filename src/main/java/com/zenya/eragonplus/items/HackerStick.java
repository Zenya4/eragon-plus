package com.zenya.eragonplus.items;

import com.zenya.eragonplus.EragonPlus;
import com.zenya.eragonplus.storage.Trigger;
import com.zenya.eragonplus.utils.LoreUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class HackerStick implements CItem {
    private static LoreUtils loreUtils = LoreUtils.getInstance();

    @Override
    public void onTrigger(Event event, Trigger trigger) {
        switch(trigger) {
            case ENTITY_DAMAGE: {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        ((LivingEntity) e.getEntity()).setNoDamageTicks(0);
                    }
                }.runTaskLater(EragonPlus.getInstance(), 1L);
            }
        }
    }

    @Override
    public String getKey() {
        return "HackerStick";
    }

    @Override
    public List<Trigger> getTriggers() {
        List<Trigger> triggers = new ArrayList<Trigger>();
        triggers.add(Trigger.ENTITY_DAMAGE);
        return triggers;
    }

    @Override
    public List<String> getPermissions() {
        List<String> permissions = new ArrayList<String>();
        permissions.add("erangonplus.admin");
        permissions.add("eragonplus.citem.hackerstick");
        return permissions;
    }

    @Override
    public String getName()  {
        return ChatColor.translateAlternateColorCodes('&', "&4Hacker Stick");
    }

    @Override
    public List<String> getDescription() {
        ArrayList<String> desc = new ArrayList<String>();
        desc.add(ChatColor.translateAlternateColorCodes('&', "&7An ordinary stick"));
        desc.add("");
        desc.add(ChatColor.translateAlternateColorCodes('&', "&5Left click to use"));
        desc.add("");
        return desc;
    }

    @Override
    public Material getMaterial() {
        return Material.STICK;
    }

    @Override
    public ItemStack getItem() {
        ItemStack item = new ItemStack(getMaterial());
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(getName());
        meta.setLore(getDescription());
        item.setItemMeta(meta);

        item = loreUtils.setKey(item, "&8ID", getKey());
        return item;
    }
}


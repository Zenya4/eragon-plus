package com.zenya.eragonplus.storage;

import com.zenya.eragonplus.items.CItem;
import com.zenya.eragonplus.items.*;

import java.util.HashMap;
import java.util.Set;

public class CItemStorage {

    private static CItemStorage instance;
    private HashMap<String, CItem> customItems = new HashMap<String, CItem>();

    private CItemStorage() {
        registerItem("Firecracker", new Firecracker());
        registerItem("GHead", new GHead());
        registerItem("HomingBow", new HomingBow());
        registerItem("Egg", new Egg());
        registerItem("RodOfSouls", new RodOfSouls());
        registerItem("HackerStick", new HackerStick());
    }

    public Set<String> getKeys() {
        return customItems.keySet();
    }

    public void registerItem(String key, CItem item) {
        customItems.put(key, item);
    }

    public CItem getCItem(String key) {
        return customItems.get(key);
    }

    public static CItemStorage getInstance() {
        if(instance == null) {
            instance = new CItemStorage();
        }
        return instance;
    }
}

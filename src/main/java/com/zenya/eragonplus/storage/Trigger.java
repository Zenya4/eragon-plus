package com.zenya.eragonplus.storage;

public enum Trigger {
    PLAYER_INTERACT,
    PROJECTILE_LAUNCH,
    PROJECTILE_HIT,
    ENTITY_SHOOT_BOW,
    ENTITY_DAMAGE
}

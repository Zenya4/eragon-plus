package com.zenya.eragonplus;

import com.zenya.eragonplus.citizens.NPCEventListener;
import com.zenya.eragonplus.commands.ToolBox;
import com.zenya.eragonplus.events.CItemListeners;
import com.zenya.eragonplus.events.Listeners;
import com.zenya.eragonplus.storage.CItemStorage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

public class EragonPlus extends JavaPlugin {

    private static EragonPlus eragonPlus;
    private static CItemStorage cItemStorage = CItemStorage.getInstance();

    public void onEnable() {
        eragonPlus = this;

        this.getCommand("toolbox").setExecutor(new ToolBox());
        Bukkit.getServer().getPluginManager().registerEvents(new Listeners(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new CItemListeners(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new NPCEventListener(), this);
        Bukkit.addRecipe(getGHeadRecipe());
    }

    public void onDisable() {

    }

    private ShapedRecipe getGHeadRecipe() {
        ShapedRecipe ghead = new ShapedRecipe(cItemStorage.getCItem("GHead").getItem());
        ghead.shape("GGG", "GHG", "GGG");
        ghead.setIngredient('G', Material.GOLD_INGOT);
        ghead.setIngredient('H', new MaterialData(Material.SKULL_ITEM, (byte) 3));
        return ghead;
    }

    public static EragonPlus getInstance() {
        return eragonPlus;
    }
}

package com.zenya.eragonplus.events;

import com.zenya.eragonplus.inventory.ToolBoxGUI;
import com.zenya.eragonplus.EragonPlus;
import com.zenya.eragonplus.items.CItem;
import com.zenya.eragonplus.storage.CItemStorage;
import com.zenya.eragonplus.storage.Trigger;
import com.zenya.eragonplus.utils.ChatUtils;
import com.zenya.eragonplus.utils.ConfigManager;
import com.zenya.eragonplus.utils.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class Listeners implements Listener {

    ConfigManager configManager = ConfigManager.getInstance();
    private static CItemStorage cItemStorage = CItemStorage.getInstance();

    // Negate lethal fall damage
    // Negate enderpearl damage
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageEvent(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        Player player = (Player) event.getEntity();

        if(PlayerUtils.hasMeta(player, "teleport-invincible")) {
            event.setCancelled(true);
        }

        if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
            if (player.getMaxHealth() - event.getDamage() <= 0) {
                event.setCancelled(true);
            }
        }
    }

    // Handle toolbox inventory
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent event) {
        if (event.getClickedInventory() != null && event.getClickedInventory().getHolder() != null) {
            Player player = (Player) event.getWhoClicked();
            ItemStack clickedItem = event.getCurrentItem();
            if (clickedItem == null || clickedItem.getType() == Material.AIR) return;

            ToolBoxGUI toolBoxGUI = ToolBoxGUI.getInstance();

            if (event.getClickedInventory().getHolder().equals(toolBoxGUI)) {
                toolBoxGUI.initItems();
                player.getInventory().addItem(clickedItem);
                ChatUtils.sendMessage(player, "&6You have been given a " + clickedItem.getItemMeta().getDisplayName());
                event.getCursor().setType(Material.AIR);
                event.setCancelled(true);
                return;
            }
        }
    }

    // Handle NoDelay worlds
    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if(!(event.getEntity() instanceof LivingEntity)) return;
        if(ConfigManager.getInstance().getNoDelayWorlds() != null && ConfigManager.getInstance().getNoDelayWorlds().size() != 0) {
            if(ConfigManager.getInstance().getNoDelayWorlds().contains(event.getEntity().getWorld().getName())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        ((LivingEntity) event.getEntity()).setNoDamageTicks(0);
                    }
                }.runTaskLater(EragonPlus.getInstance(), 1L);
            }
        }
    }

    // Block console-only commands
    @EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        if(ConfigManager.getInstance().getConsoleOnlyCommands() != null && ConfigManager.getInstance().getConsoleOnlyCommands().size() != 0) {
            for (String cmd : ConfigManager.getInstance().getConsoleOnlyCommands()) {
                if (event.getMessage().startsWith(cmd) || event.getMessage().startsWith("/" + cmd)) {
                    event.setCancelled(true);
                    ChatUtils.sendMessage(event.getPlayer(), "&4This command can only be executed from the console!");
                }
            }
        }
    }

    // Block enderpearl damage
    @EventHandler
    public void onPlayerTeleportEvent(PlayerTeleportEvent event) {
        Player player = event.getPlayer();

        if(event.getCause().equals(PlayerTeleportEvent.TeleportCause.ENDER_PEARL)) {
            if(!player.getWorld().getName().equalsIgnoreCase(configManager.getMainWorld())) {
                PlayerUtils.clearMeta(player, "teleport-invincible");
                PlayerUtils.setMeta(player, "teleport-invincible", "");
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        PlayerUtils.clearMeta(player, "teleport-invincible");
                    }
                }.runTaskLater(EragonPlus.getInstance(), 1L);
            }
        }
    }

    // Override GHeads
    @EventHandler
    public void onGapple(PlayerInteractEvent event) {
        ItemStack gapple = event.getPlayer().getItemInHand();
        if(gapple != null &&
                gapple.getType().equals(Material.GOLDEN_APPLE) &&
                gapple.getItemMeta() != null &&
                gapple.getItemMeta().hasDisplayName() &&
                gapple.getItemMeta().getDisplayName().equals(ChatUtils.translateColor("&6Golden Head"))) {
            CItem cItem = cItemStorage.getCItem("GHead");
            cItem.onTrigger(event, Trigger.PLAYER_INTERACT);
        }
    }
}

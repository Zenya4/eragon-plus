package com.zenya.eragonplus.events;

import com.zenya.eragonplus.storage.Trigger;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class CItemListeners implements Listener {
    private static CItemHandlers cItemHandlers = CItemHandlers.getInstance();

    @EventHandler
    public void onProjectileLaunchEvent(ProjectileLaunchEvent event) {
        if (!(event.getEntity().getShooter() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity().getShooter();
        cItemHandlers.handleEvent(player.getInventory().getItemInHand(), event, Trigger.PROJECTILE_LAUNCH);
    }

    @EventHandler
    public void onProjectileHitEvent(ProjectileHitEvent event) {
        Entity entity = event.getEntity();
        if(entity.getCustomName() != null) {
            if(entity.getCustomName().equals("Firecracker") || entity.getCustomName().equals("Egg")) {
                cItemHandlers.handleEvent(entity, event, Trigger.PROJECTILE_HIT);
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        cItemHandlers.handleEvent(player.getInventory().getItemInHand(), event, Trigger.PLAYER_INTERACT);
    }

    @EventHandler
    public void onEntityShootBowEvent(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        cItemHandlers.handleEvent(player.getInventory().getItemInHand(), event, Trigger.ENTITY_SHOOT_BOW);
    }

    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        Entity entity = event.getDamager();

        if(entity instanceof Player) {
            cItemHandlers.handleEvent(((Player) entity).getInventory().getItemInHand(), event, Trigger.ENTITY_DAMAGE);
        }

        if(entity.getCustomName() != null && (entity.getCustomName().equals("Egg") || entity.getCustomName().equals("RodOfSouls"))) {
            cItemHandlers.handleEvent(entity, event, Trigger.ENTITY_DAMAGE);
        }
    }
}
